module "eventbridge" {
  source = "terraform-aws-modules/eventbridge/aws"

  create_bus    = false
  create_role   = false
  create_rules  = true

  rules = {
    crons = {
      description         = "Invoke myStopinator Lambda function every minute"
      schedule_expression = "rate(1 minute)"
    }
  }

  targets = {
    crons = [
      {
        name            = "myStopinator Lambda function"
        arn             = aws_lambda_function.mystopinator_lambda.arn
      }
    ]
  }
}