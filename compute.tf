data "archive_file" "mystopinator_code" {
  type          = "zip"
  source_file   = "./mystopinator/main.py"
  output_path   = "mystopinator.zip"
}


data "aws_iam_role" "myStopinatorRole" {
  name = "myStopinatorRole"
}

resource "aws_lambda_function" "mystopinator_lambda" {
  filename      = "mystopinator.zip"
  function_name = "mystopinator_lambda" # name of lambda function
  role          = data.aws_iam_role.myStopinatorRole.arn
  handler       = "main.lambda_handler" # python function to be called to handle events

  source_code_hash = data.archive_file.mystopinator_code.output_base64sha256
  runtime = "python3.8"
}

resource "aws_lambda_function_url" "test_mystopinator_lambda" {
  function_name      = aws_lambda_function.mystopinator_lambda.function_name
  authorization_type = "NONE"
}

output "aws_lambda_function_url" {
  value = aws_lambda_function_url.test_mystopinator_lambda.function_url
}
